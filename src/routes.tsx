import * as React from 'react';
import { Route } from 'react-router-dom';
import { Home } from './components/Home';
import App from './App';
import { Layout } from './components/Layout';
import { FetchEmployee } from './components/FetchEmployee';
import { AddEmployee } from './components/AddEmployee';
import { FormExample } from './components/FormExample';
import { FormExample2 } from './components/FormExample2';
import { CustomerList } from './components/CustomerList';
import { AddCustomer } from './components/AddCustomer';
import { Login } from './components/accounts/Login';

export const routes =

    <Layout>
        <Route exact={true} path='/' component={App} />
        <Route path='/fetchemployee' component={FetchEmployee} />
        <Route path='/home' component={Home} />
        <Route path='/addemployee' component={AddEmployee} />
        <Route path='/employee/edit/:empid' componenet={AddEmployee} />
        <Route path='/employee/edit/:empid' component={AddEmployee} />
        <Route path='/formexample' component={FormExample} />
        <Route path='/formexample2' component={FormExample2} />
        <Route path='/login' component={Login} />
        <Route path='/addcustomer' component={AddCustomer} />
        <Route path='/customerlist' component={CustomerList} />

    </Layout>;