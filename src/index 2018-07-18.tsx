import './css/site.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter, Route } from 'react-router-dom';
import * as RoutesModule from './routes';
import 'font-awesome/css/font-awesome.min.css';

let routes = RoutesModule.routes;

function renderApp() {
    // This code starts up the React app when it runs in a browser. It sets up the routing
    // configuration and injects the app into a DOM element.
    const bases = document.getElementsByTagName('base');
    let baseUrl = "";

    if (bases.length > 0) {
        debugger;
        baseUrl = bases[0].href;
    }
    debugger;
    ReactDOM.render(
        <AppContainer>
            <BrowserRouter>
                <div>
                    <Route children={routes} basename={baseUrl} />
                </div>
            </ BrowserRouter>
        </AppContainer>,
        document.getElementById('root') as HTMLElement
    );
    window.scroll(0, 0);
}

renderApp();

// Allow Hot Module Replacement (module as any) 
if (module.hot) {
    module.hot.accept('./routes', () => {
        routes = require<typeof RoutesModule>('./routes').routes;
        renderApp();
    });
}