﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ConfigClass } from '../constraints/config';
import Pagination from "react-js-pagination";

interface FetchEmployeeDataState {
    empList: EmployeeData[];
    loading: boolean;
    activePage: number;
    selectedOption: string;
}

const perPage = 5;
let totalRecords = 0;

export class FetchEmployee extends React.Component<RouteComponentProps<{}>, FetchEmployeeDataState> {
    WebApi: any;
    sortColumnName: string = "";
    sortOrder: any = null;
    txtSearch: string = "";

    constructor(props: any) {
        super(props);

        this.WebApi = ConfigClass.WebApi;

        this.state = { empList: [], loading: true, activePage: 1, selectedOption: "" };
        this.handleGetData(this.state.activePage);

        // This binding is necessary to make "this" work in the callback
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleGetData = this.handleGetData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeChkBox = this.handleChangeChkBox.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
    }

    handleGetData(pageNumber: number) {
        debugger;
        fetch(this.WebApi + 'api/Employee/Index')
            .then(response => response.json() as Promise<EmployeeData[]>)
            .then(data => {

                totalRecords = data.length;
                const perPageRecord = [];
                for (let i = 0; i < perPage * pageNumber; i++) {
                    if (totalRecords > i) {
                        perPageRecord.push(data[i]);
                    }
                }

                this.setState({ empList: perPageRecord, loading: false });
                window.scroll(0, 0);
            });
    }

    handlePageChange(pageNumber: number) {

        console.log(`The page number is : ${pageNumber} ${totalRecords}`);

        this.handleGetData(pageNumber);
        debugger;
        this.setState({ activePage: pageNumber });
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderEmployeeTable(this.state.empList);

        return <div>
            <h1>Employee Data</h1>
            <p>This component demonstrates fetching Employee data from the server.</p>
            <p>
                <Link to="/addemployee">Create New</Link>
            </p>
            {contents}
        </div>;
    }

    // Handle Delete request for an employee
    private handleDelete(id: number): any {
        debugger;
        if (!confirm("Do you want to delete employee with Id: " + id)) {
            return;
        }
        else {
            fetch(this.WebApi + 'api/Employee/Delete/' + id, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        empList: this.state.empList.filter((rec) => {
                            return (rec.EmployeeId != id);
                        })
                    });
            });
        }
    }

    private handleEdit(id: number): any {
        debugger;
        this.props.history.push("/employee/edit/" + id);
    }

    private handleChange(event: any) {

        event.preventDefault();
        this.txtSearch = event.target.value;

        fetch(this.WebApi + 'api/Employee/Search/?search=' + this.txtSearch)
            .then(response => response.json() as Promise<EmployeeData[]>)
            .then(data => {

                totalRecords = data.length;
                const perPageRecord = [];
                for (let i = 0; i < perPage * this.state.activePage; i++) {
                    if (totalRecords > i) {
                        perPageRecord.push(data[i]);
                    }
                }

                if (totalRecords > 0) {

                    this.setState({ empList: perPageRecord, loading: false });
                }
                else {
                    this.setState({ empList: perPageRecord });
                }
                window.scroll(0, 0);
            });
    }

    private sortChanged(sortColumnName: string, order: any, event: any) {
        debugger;
        event.preventDefault();
        this.sortColumnName = sortColumnName;
        //this.state.Data.currentPage = 1;
        this.sortOrder = order == 'asc' ? 'desc' : 'asc';
        this.populateData();
    }

    private _sortClass(filterName: string) {

        return "fa fa-fw " + ((filterName == this.sortColumnName) ? ("fa-sort-" + this.sortOrder) : "fa-sort");
    }

    private populateData() {
        debugger;
        var params = {
            pageSize: perPage,
            currentPage: this.state.activePage,
            sortColumnName: "",
            sortOrder: "",
            search: this.txtSearch
        }
        if (this.sortColumnName) {
            params.sortColumnName = this.sortColumnName;
        }
        if (this.sortOrder) {
            params.sortOrder = this.sortOrder;
        }
        debugger;
        fetch(this.WebApi + 'api/Employee/IndexByParams/?sortColumnName=' + params.sortColumnName + '&sortOrder=' + params.sortOrder + '&pageSize=' + params.pageSize + '&currentPage=' + params.currentPage + '&search=' + params.search)
            .then(response => response.json() as Promise<EmployeeData[]>)
            .then(data => {

                totalRecords = data.length;

                const perPageRecord = [];
                for (let i = 0; i < perPage * params.currentPage; i++) {
                    if (totalRecords > i) {
                        perPageRecord.push(data[i]);
                    }
                }

                this.setState({ empList: perPageRecord, loading: false });
                window.scroll(0, 0);
            });
    }

    private handleChangeChkBox(employeeId: number, status: boolean): any {
        debugger;
        //this.AddForm.IsActive = !this.AddForm.IsActive;
        if (!confirm("R u sure to change the status: " + employeeId))
            return;
        else {
            let params: any = {};
            params.EmployeeId = employeeId;
            params.Status = status;
            const request = new Request(this.WebApi + 'api/Employee/Status', {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
                body: JSON.stringify(params)
            });

            return fetch(request).then(response => {
                return response.json();
            }).then((responseJson) => {
                this.props.history.push("/fetchemployee");
            });
        }
    }

    private handleChangeSelect(event: any) {
        debugger;
        if (event.target.selectedOptions[0].label == "Please select") {
            this.setState({ selectedOption: event.target.value });
        }
        else if (event.target.selectedOptions[0].label == "Edit") {
            this.handleEdit(event.target.value);
            this.setState({ selectedOption: event.target.value });
        }
        else if (event.target.selectedOptions[0].label == "Delete") {
            this.handleDelete(event.target.value);
            this.setState({ selectedOption: "0" });
        }
    }

    // Returns the HTML table to the render() method.

    private renderEmployeeTable(empList: EmployeeData[]) {

        debugger;
        return (
            <div>
                <div className="row">
                    <div style={{ paddingLeft: '15px' }} className="form-group row" >
                        <label className="control-label col-md-12" htmlFor="Search">Search</label>
                        <div className="col-md-4">
                            <input className="form-control" type="text" name="Search" ref={this.txtSearch} defaultValue={this.txtSearch} onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <table className='table'>
                        <thead>
                            <tr>
                                <th></th>
                                <th onClick={this.sortChanged.bind(this, 'EmployeeId', this.sortOrder)}>
                                    EmployeeId
                                    <i className={this._sortClass('EmployeeId')}></i>
                                </th>
                                <th onClick={this.sortChanged.bind(this, 'Name', this.sortOrder)}>
                                    Name
                                    <i className={this._sortClass('Name')}></i>
                                </th>
                                <th onClick={this.sortChanged.bind(this, 'Gender', this.sortOrder)}>Gender
                                    <i className={this._sortClass('Gender')}></i>
                                </th>
                                <th onClick={this.sortChanged.bind(this, 'Department', this.sortOrder)}>Department
                                    <i className={this._sortClass('Department')}></i>
                                </th>
                                <th onClick={this.sortChanged.bind(this, 'City', this.sortOrder)}>City
                                    <i className={this._sortClass('City')}></i>
                                </th>
                                <th>
                                    Is Active
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {!empList.length && (
                                <span>No Results Found</span>
                            )}
                            {empList.map((emp, index) =>
                                <tr key={index}>
                                    <td></td>
                                    <td>{emp.EmployeeId}</td>
                                    <td>{emp.Name}</td>
                                    <td>{emp.Gender}</td>
                                    <td>{emp.Department}</td>
                                    <td>{emp.City}</td>
                                    {(emp.IsActive == true) ? <td> true </td>
                                        : <td> false </td>
                                    }
                                    {(emp.IsActive == true) ? <td> <input type="checkbox" name="IsActive" onChange={(id) => this.handleChangeChkBox(emp.EmployeeId, false)} defaultChecked={emp.IsActive} /> </td>
                                        : <td> <input type="checkbox" name="IsActive" onChange={(id) => this.handleChangeChkBox(emp.EmployeeId, true)} defaultChecked={emp.IsActive} /> </td>
                                    }
                                    <td>
                                        <select className="form-control" data-val="true" name="City" onChange={this.handleChangeSelect} value={this.state.selectedOption} >
                                            <option value={0} >Please select</option>
                                            <option value={emp.EmployeeId}>Edit</option>
                                            <option value={emp.EmployeeId}>Delete</option>
                                        </select>
                                        <a className="action" onClick={(id) => this.handleEdit(emp.EmployeeId)}>Edit</a>  |
                                        <a className="action" onClick={(id) => this.handleDelete(emp.EmployeeId)}>Delete</a>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
                <div className="panel-body">
                    <div className='text-center'>
                        <Pagination
                            prevPageText='prev'
                            nextPageText='next'
                            firstPageText='first'
                            lastPageText='last'
                            activePage={this.state.activePage}
                            itemsCountPerPage={perPage}
                            totalItemsCount={totalRecords}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

//<select className="form-control" data-val="true" >
//    <option value="">-- Select options --</option>
//    <option onClick={(id) => this.handleEdit(emp.EmployeeId)} value="Edit">Edit</option>
//    <option onClick={(id) => this.handleDelete(emp.EmployeeId)} value="Delete">Delete</option>
//</select>
export class EmployeeData {
    EmployeeId: number = 0;
    Name: string = "";
    Gender: string = "";
    City: string = "";
    Department: string = "";
    IsActive: boolean;
}