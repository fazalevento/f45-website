﻿import 'rc-time-picker/assets/index.css';
import 'react-datepicker/dist/react-datepicker.css';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { ConfigClass } from '../constraints/config';
import DatePicker from 'react-datepicker';
import * as moment from "moment";

var TimePicker = require("rc-time-picker/es/TimePicker").default;
//var Form = require('react-validation/build/form').default;
//var Input = require('react-validation/build/input').default;

//const required = (value: string): any => {
//    debugger;
//    if (!value.toString().trim().length) {
//        // We can return string or jsx as the 'error' prop for the validated Component
//        return 'require';
//    }
//};

interface AddEmployeeDataState {
    title: string;
    loading: boolean;
    cityList: Array<any>;
    //  empData: EmployeeData;
}

const timeFormat = 'HH:mm';
const timeNow = moment().hour(0).minute(0);
const dateFormat = 'YYYY-MM-DD';
export class AddEmployee extends React.Component<RouteComponentProps<{}>, AddEmployeeDataState> {

    props: any;
    WebApi: any;

    GetFormValues: any = {
        getName: "",
        getGender: "",
        getDept: "",
        getCity: "",
        getIsActive: ""
    };

    AddForm: any = {
        EmployeeId: 0,
        Name: "",
        Gender: "",
        City: "",
        Department: "",
        IsActive: false,
        CreateDate: moment().format(dateFormat),
        CreateTime: moment(),//.hour(0).minute(0)//.format(timeFormat)
        OriginalTime: moment()//.format(timeFormat)
    };
    constructor(props: any) {
        super(props);
        this.WebApi = ConfigClass.WebApi;

        this.state = { title: "", loading: true, cityList: [] };
        fetch(this.WebApi + 'api/Employee/GetCityList')
            .then(response => response.json() as Promise<Array<any>>)
            .then(data => {
                this.setState({ cityList: data });
            });
        debugger;
        var empid = this.props.match.params["empid"];

        // This will set state for Edit employee
        if (empid > 0) {
            debugger;
            fetch(this.WebApi + 'api/Employee/Details/' + empid)
                .then(response => response.json())
                .then(data => {
                    debugger;
                    this.AddForm = data;
                    console.log("Model =>  " + this.AddForm.CreateTime);
                    //console.log("Model 2 =>  " + this.AddForm.CreateTime.moment().hour(0).minute(0));
                    console.log("Time =>  " + timeNow);
                    this.setState({ title: "Edit", loading: false });
                });
        }

        // This will set state for Add employee
        else {
            this.state = { title: "Create", loading: false, cityList: [] };
        }

        // This binding is necessary to make "this" work in the callback
        this.handleChange = this.handleChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeChkBox = this.handleChangeChkBox.bind(this);
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm(this.state.cityList);

        return <div>
            <h1>{this.state.title}</h1>
            <h3>Employee</h3>
            <hr />
            {contents}
        </div>;
    }

    // This will handle the submit form event.
    private handleSave(event: any): any {
        debugger;
        event.preventDefault();
        debugger;
        this.AddForm.Name = this.GetFormValues.getName.value;
        this.AddForm.Gender = this.GetFormValues.getGender.value;
        this.AddForm.Department = this.GetFormValues.getDept.value;
        this.AddForm.City = this.GetFormValues.getCity.value;
        //this.AddForm.IsActive = this.GetFormValues.getIsActive.value;
        //this.AddForm.CreateDate = this.GetFormValues.getCreateDate.value;
        this.AddForm.CreateTime = this.AddForm.CreateTime.format(timeFormat);

        const data = this.AddForm;

        // PUT request for Edit employee.
        if (this.AddForm.EmployeeId) {
            debugger;
            fetch(this.WebApi + 'api/Employee/Edit', {
                method: 'PUT',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(data)
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchemployee");
                })
        }

        // POST request for Add employee.
        else {
            debugger;           
            const request = new Request(this.WebApi + 'api/Employee/Create', {
                method: 'POST',
                // mode: 'no-cors',             
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
                body: JSON.stringify(data)
            });
            return fetch(request).then(response => {
                return response.json();
            }).then((responseJson) => {
                this.props.history.push("/fetchemployee");
            });
            //}).catch(error => {
            //    return error;
            //});
        }
    }

    // This will handle Cancel button click event.
    private handleCancel(event: any) {
        event.preventDefault();
        this.props.history.push("/fetchemployee");
    }

    private handleChangeTime(time: any) {
        debugger;
        this.AddForm.CreateTime = time; 
     
        this.setState({
            loading: false
        });
    }

    private handleChange(date: any) {
        debugger;

        this.AddForm.CreateDate = date.format(dateFormat);
        this.setState({
            loading: false
        });        
    }

    private handleChangeChkBox() {
        debugger;
        this.AddForm.IsActive = !this.AddForm.IsActive;
        console.log(this.AddForm.IsActive);
        if (this.AddForm.IsActive == false) {
            this.AddForm.CreateDate = null;
        }
        this.setState({
            loading: false
        });
    }


    // Returns the HTML Form to the render() method.
    private renderCreateForm(cityList: Array<any>) {
        debugger;
        return (
            <form onSubmit={this.handleSave.bind(this)}>
                <div className="form-group row" >
                    <input type="hidden" name="employeeId" value={this.AddForm.EmployeeId} />
                </div>
                <div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Name">Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Name" ref={(input) => this.GetFormValues.getName = input}
                            defaultValue={this.AddForm.Name} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Gender">Gender</label>
                    <div className="col-md-4">
                        <select className="form-control" data-val="true" name="Gender" ref={(input) => this.GetFormValues.getGender = input} defaultValue={this.AddForm.Gender} required>
                            <option value="">-- Select Gender --</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Department" >Department</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Department" ref={(input) => this.GetFormValues.getDept = input} defaultValue={this.AddForm.Department} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="IsActive" >Status</label>
                    <div className="col-md-1">
                        <input className="form-control" type="checkbox" name="IsActive" onChange={this.handleChangeChkBox} checked={this.AddForm.IsActive} />
                    </div>
                </div>


                {(this.AddForm.IsActive == true)
                    ? <div>
                        <div className="form-group row">
                            <label className="control-label col-md-12" htmlFor="Date" >Select Date</label>
                            <div className="col-md-4">
                                <DatePicker
                                    className="form-control"
                                    value={this.AddForm.CreateDate}
                                    onChange={this.handleChange}
                                    dateFormat={dateFormat}
                                />
                            </div>
                        </div>
                    </div>
                    : null
                }
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Time" >Enter Time</label>
                    <div className="col-md-4">
                        <TimePicker
                            showSecond={false}
                            defaultValue={timeNow}
                            value={timeNow}//{this.AddForm.OriginalTime}
                            // className="form-control"
                            onChange={this.handleChangeTime}
                        //format={timeFormat}
                        //inputReadOnly
                        />
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="City">City</label>
                    <div className="col-md-4">
                        <select className="form-control" data-val="true" name="City" ref={(input) => this.GetFormValues.getCity = input} defaultValue={this.AddForm.City} required>
                            <option value="">-- Select City --</option>
                            {cityList.map((city, index) =>
                                <option key={city.CityId} value={city.CityName}>{city.CityName}</option>
                            )}
                        </select>
                    </div>
                </div>

                <div className="form-group">
                    <button type="submit" className="btn btn-primary">Save</button>
                    <button className="btn btn-warning" onClick={this.handleCancel}>Cancel</button>
                </div>
            </form>
        );
    }
}



