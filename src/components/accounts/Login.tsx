import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { ServiceResponse } from '../common/ServiceResponse';
import logo from '../../images/login/logo.png';

interface LoginData {
    ServiceResponse: ServiceResponse[];
}

export class Login extends React.Component<RouteComponentProps<{}>, LoginData> {

    GetFormModel: any = {
        Email: "",
        Password: ""
    };

    SetFormModel: any = {
        Email: "",
        Password: ""
    }

    constructor(props: any) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    private submitForm(event: any): any {
        debugger;
        event.preventDefault();
        this.SetFormModel.Email = this.GetFormModel.Email.value;
        this.SetFormModel.Password = this.GetFormModel.Password.value;

        const data = this.SetFormModel;
        debugger;
        const request = new Request('http://eventoclients.com:8063/api/Login', {
            method: 'POST',
            // mode: 'no-cors',             
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(data)
        });
        return fetch(request).then(response => {
            return response.json();
        }).then(ServiceResponse => {
            this.setState({ ServiceResponse: ServiceResponse })
        })
            .catch(err => console.error(request, err.toString()))

        //.then((responseJson) => {
        //        this.props.history.push("/fetchemployee");
        //    });
        //}).catch(error => {
        //    return error;
        //});
    }

    public render() {

        return <div>
            <div className='logo'>
                <img src={logo} alt='login-image' />
            </div>
            <div className='info'>
                <p>Sign into your F45 account</p>
            </div>
            <form onSubmit={this.submitForm.bind(this)} className='form-wrapp'>
                <div className='form-group'>
                    <div className='box'>
                        <label className='label-control'>LOGIN</label>
                        <input type='text' className='form-control' placeholder='User Name' ref={(input) => this.GetFormModel.Email = input} required />
                    </div>
                </div>
                <div className='form-group'>
                    <div className='box'>
                        <label className='label-control'>PASSWORD</label>
                        <input type='password' className='form-control' placeholder='Credentials' ref={(input) => this.GetFormModel.Password = input} required />
                    </div>
                </div>
                <div className='form-group help-block'>
                    <div className='row'>
                        <div className='col-xs-6'>
                            <label>
                                <input type='checkbox' /> Keep me Signed in
                            </label>
                        </div>
                        <div className='col-xs-6'>
                            <a href='#.'>Forgot Password?</a>
                        </div>
                    </div>
                </div>
                <div className='form-group inline-btns'>
                    <input type='submit' className='btn btn-theme blue' value='Log In' />
                    <a href='#.' className='btn btn-theme white'>Create Account</a>
                </div>
                <div className='form-group'>
                    <div className='other-account'>
                        <p>Sign in with <a href='#.'>Facebook</a> or <a href='#.'>Google</a></p>
                    </div>
                </div>
            </form>
        </div>;
    }
}
