﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Form, FormControl, FormGroup, ControlLabel, HelpBlock } from 'react-bootstrap';
//import Form from 'react-bootstrap/lib/Navbar';

interface AddFormExample {
    value: string;
    Test: string;
}

export class FormExample extends React.Component<RouteComponentProps<{}>, AddFormExample> {

    constructor(props: any, context: any) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);
        this.handleChange1 = this.handleChange1.bind(this);

        this.state = {
            value: '',
            Test: ''
        };
    }

    getValidationState() {
        debugger;

        const length = this.state.value.length;
        if (length > 10) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
        return null;
    }

    getValidationState1() {
        debugger;
        const txt = this.state.Test;
        if (txt.length > 0 && txt.length < 3) {
            return 'error';
        }
        else if (txt.length >= 3) {
            return 'success';
        }
        return null;
    }

    handleChange(e: any) {
        this.setState({ value: e.target.value });
    }

    handleChange1(e: any) {
        this.setState({ Test: e.target.value });
    }

    render() {
        debugger;
        return (
            <Form>
                <FormGroup
                    className="col-sm-4"
                    controlId="formBasicText"
                    validationState={this.getValidationState()}
                >
                    <ControlLabel>Working example with validation</ControlLabel>
                    <FormControl
                        type="text"
                        //value={this.state.value}
                        ref={this.state.value}
                        defaultValue="Testing"
                        placeholder="Enter text"
                        onChange={this.handleChange}
                    />
                    <FormControl.Feedback />
                    {this.getValidationState() == 'error' ? <HelpBlock>Validation is failed</HelpBlock>
                        : <HelpBlock>Validation is successed </HelpBlock>
                    }
                </FormGroup>

                <FormGroup
                    className="col-sm-6"
                    controlId="TestText"
                    validationState={this.getValidationState1()}

                >
                    <ControlLabel>Testing Validation</ControlLabel>
                    <FormControl
                        type="text"
                        rel={this.state.Test}
                        defaultValue="2nd Test"
                        onChange={this.handleChange1}
                    />
                    <FormControl.Feedback />
                    {this.getValidationState1() == 'error' ? <HelpBlock>Validation is failed</HelpBlock>
                        : null
                    }
                </FormGroup>
            </Form>
        );
    }
}
