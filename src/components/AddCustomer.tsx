﻿import 'rc-time-picker/assets/index.css';
import 'react-datepicker/dist/react-datepicker.css';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import DatePicker from 'react-datepicker';
import * as moment from "moment";
// var TimePicker = require("rc-time-picker/es/TimePicker").default;


// const timeNow = moment();
const dateFormat = 'YYYY-MM-DD';
const DateNow = moment().format(dateFormat);

export class AddCustomer extends React.Component<RouteComponentProps<{}>, {}> {

    constructor(props: any) {
        super(props);

        // This binding is necessary to make "this" work in the callback
        this.handleChange = this.handleChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeChkBox = this.handleChangeChkBox.bind(this);
    }

    public render() {
        let contents = this.renderCreateForm();

        return <div className='content'>
            <div className='page-title'>
            <h1>ADD NEW CUSTOMER</h1>
            </div>
            {contents}
        </div>;
    }

    // This will handle the submit form event.
    private handleSave(event: any): any {

    }

    // This will handle Cancel button click event.
    private handleCancel(event: any) {
        event.preventDefault();
        this.props.history.push("/customerlist");
    }

    private handleChangeTime(time: any) {
        debugger;

    }

    private handleChange(date: any) {
        debugger;


    }

    private handleChangeChkBox() {
        debugger;

    }


    // Returns the HTML Form to the render() method.
    private renderCreateForm() {
        debugger;
        return (
            <form onSubmit={this.handleSave.bind(this)} >
                <div className="form-group row" >
                    <input type="hidden" name="employeeId" />
                </div>
                <div className='row'>
                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className=" label-control" htmlFor="Name">Name</label>
                        <input className="form-control" type="text" name="Name" placeholder='Enter your First Name' required />
                    </div>
                        </div> 
                    </div>


                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className=" label-control" htmlFor="LastName">LAST NAME</label>
                        <input className="form-control" type="text" name="LastName" placeholder='Enter your Last Name' required />
                    </div>
                        </div> 
                    </div>

                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className=" label-control" htmlFor="LastName">DATE OF BIRTH</label>
                        <DatePicker
                            className="form-control"
                            placeholderText='Select Date of Birth'
                            value={DateNow}
                            onChange={this.handleChange}
                            dateFormat={dateFormat}
                        />
                        <i className="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                        </div> 
                    </div>

                    </div>
                    <div className='row'>

                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="Gender">Gender</label>
                        <select className="form-control" data-val="true" name="Gender" required>
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                        </div> 
                    </div>


                    


                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="email" >EMAIL</label>
                        <input className="form-control" type="email" placeholder='email@example.com' name="email" required />
                    </div>
                        </div> 
                    </div>

                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="CONTACT" >CONTACT</label>
                        <input className="form-control" type="text" placeholder='Enter Contact Number' name="CONTACT" required />
                    </div>
                        </div> 
                    </div>

                    
                </div>


                <div className='row'>
                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="PASSWORD" >ENTER NEW PASSWORD</label>
                        <input className="form-control" type="password" placeholder='Enter New Password' name="PASSWORD" required />

                            {/* <DatePicker
                            className="form-control"
                            placeholderText='Select Date of Birth'
                            value={DateNow}
                            onChange={this.handleChange}
                            dateFormat={dateFormat}
                        /> */}
                        {/* <input className="form-control" type="checkbox" name="IsActive" /> */}
                    </div>
                        </div> 
                    </div>

                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="REPEAT" >REPEAT NEW PASSWORD</label>
                        <input className="form-control" type="password" placeholder='Repeat New Password' name="REPEAT" required />
                    </div>
                        </div> 
                    </div>


                    <div className='col-md-4'>
                        <div className='form-group'>
                            <input type='button' className='btn btn-theme gray margin-12' value='Generate Password' />
                        </div> 
                    </div>


                </div>



                <div className='row'>
                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="amount" >INITIAL ACCOUNT BALANCE</label>
                        <input className="form-control" type="password" placeholder='Enter value in Euros €' name="amount" required />
                    </div>
                        </div> 
                    </div>


                </div>

                <div className='page-title inner-title'>
               <h1>SUBSCRIPTION PLAN</h1> 
                </div>

                <div className='row'>
                    <div className='col-md-4'>
                        <div className='form-group'>
                        <div className='box'>
                        <label className="label-control" htmlFor="PLAN" >SUBSCRIPTION PLAN</label>
                        <select className="form-control" data-val="true" name="PLAN" required>
                            <option value="">Select Subscription Plan</option>
                            <option value="Male">Select Subscription Plan</option>
                            <option value="Female">Select Subscription Plan</option>
                        </select>
                    </div>
                        </div> 
                    </div>


                </div>



            <div className="form-group">
                <button type="submit" className="btn btn-theme blue">Add New Customer</button>
            </div>
            </form >
        );
    }
}



