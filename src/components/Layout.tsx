import * as React from 'react';
import { NavMenu } from './NavMenu';
import { LoginNavMenu } from './LoginNavMenu';
import { Header } from './Header';
import { Footer } from './Footer';

export interface LayoutProps {
    children?: React.ReactNode;
}
const flag: boolean = true;

export class Layout extends React.Component<LayoutProps, {}> {
    public render() {
        return <div className='container-fluid'>
            <div className='row'>
                {(flag == true) ?
                    <div>
                        <div className='col-sm-12' >
                            <Header />
                        </div>
                        <div className='col-sm-3'>
                            <NavMenu />
                        </div>
                        <div className='col-sm-9'>
                            {this.props.children}
                        </div>
                        <div className='col-sm-12' >
                            <Footer />
                        </div>
                    </div>
                    :
                    <div className="login">
                        <div className='left'>
                            <LoginNavMenu />
                        </div>
                        <div className='right'>
                            {this.props.children}
                        </div>
                    </div>
                }
            </div>
        </div>;
    }
}
