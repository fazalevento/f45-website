﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import Pagination from "react-js-pagination";

export class CustomerList extends React.Component<RouteComponentProps<{}>, {}> {


    constructor(props: any) {
        super(props);

        this.handlePageChange = this.handlePageChange.bind(this);
        
    }

    public render() {
        let contents = this.renderEmployeeTable();

        return <div className='content'>
        <div className='row'>
            <div className='col-sm-6'>
            <div className='page-title'>
            <h1>MANAGE CUSTOMERS <small><Link to="/addcustomer" className='btn  gray btn-sm'>Add New</Link></small></h1>
            </div>
            </div>
            <div className='col-sm-6 search'>
            <input className="form-control" type="text" placeholder='Search Customers' name="Search" />
            </div>
        </div>
        
            {contents}
        </div>;
    }

    private handlePageChange(pageNumber: number) {

    }

    // Returns the HTML table to the render() method.

    private renderEmployeeTable() {

        debugger;
        return (
            <div>
                <div className="row">
                <div className='col-md-12'>
                <div className='table-responsive'>
                    <table className='table text-center margin-12 table-striped'>
                        <thead>
                            <tr>
                                <th>
                                ID
                                   
                                </th>
                                <th>
                                NAME

                                </th>
                                <th>
                                SUBSCRIPTION
                                </th>
                                <th >
                                BALANCE / VALIDITY
                                </th>
                                <th>STATUS
                                </th>
                                <th>
                                ACTIONS
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>John Doe</td>
                                <td>Session Based</td>
                                <td>10 Sessions</td>                                
                                <td>  
                                    <label className="switch"> <input type="checkbox" /> <span className="slider round"></span> </label> </td>
                                <td>
                                    <select className="form-control" data-val="true" name="City" >
                                        <option value={0} >Please select</option>
                                        <option value={1}>Edit</option>
                                        <option value={2}>Delete</option>
                                        <option value={3}>Subscription</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>John Doe</td>
                                <td>Session Based</td>
                                <td>10 Sessions</td>                                
                                <td>  
                                    <label className="switch"> <input type="checkbox" /> <span className="slider round"></span> </label> </td>
                                <td>
                                    <select className="form-control" data-val="true" name="City" >
                                        <option value={0} >Please select</option>
                                        <option value={1}>Edit</option>
                                        <option value={2}>Delete</option>
                                        <option value={3}>Subscription</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>John Doe</td>
                                <td>Session Based</td>
                                <td>10 Sessions</td>                                
                                <td>  
                                    <label className="switch"> <input type="checkbox" /> <span className="slider round"></span> </label> </td>
                                <td>
                                    <select className="form-control" data-val="true" name="City" >
                                        <option value={0} >Please select</option>
                                        <option value={1}>Edit</option>
                                        <option value={2}>Delete</option>
                                        <option value={3}>Subscription</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>John Doe</td>
                                <td>Session Based</td>
                                <td>10 Sessions</td>                                
                                <td>  
                                    <label className="switch"> <input type="checkbox" /> <span className="slider round"></span> </label> </td>
                                <td>
                                    <select className="form-control" data-val="true" name="City" >
                                        <option value={0} >Please select</option>
                                        <option value={1}>Edit</option>
                                        <option value={2}>Delete</option>
                                        <option value={3}>Subscription</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>John Doe</td>
                                <td>Session Based</td>
                                <td>10 Sessions</td>                                
                                <td>  
                                    <label className="switch"> <input type="checkbox" /> <span className="slider round"></span> </label> </td>
                                <td>
                                    <select className="form-control" data-val="true" name="City" >
                                        <option value={0} >Please select</option>
                                        <option value={1}>Edit</option>
                                        <option value={2}>Delete</option>
                                        <option value={3}>Subscription</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>John Doe</td>
                                <td>Session Based</td>
                                <td>10 Sessions</td>                                
                                <td>  
                                    <label className="switch"> <input type="checkbox" /> <span className="slider round"></span> </label> </td>
                                <td>
                                    <select className="form-control" data-val="true" name="City" >
                                        <option value={0} >Please select</option>
                                        <option value={1}>Edit</option>
                                        <option value={2}>Delete</option>
                                        <option value={3}>Subscription</option>
                                    </select>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    </div>
                </div>
                <div className="panel-body">
                    <div className='text-center'>
                        <Pagination
                            prevPageText='prev'
                            nextPageText='next'
                            firstPageText='first'
                            lastPageText='last'
                            activePage={1}
                            itemsCountPerPage={3}
                            totalItemsCount={10}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
