import * as React from 'react';
import { findDOMNode } from 'react-dom';
import * as $ from 'jquery';

import 'script-loader!smartadmin-plugins/bower_components/bootstrapvalidator/dist/js/bootstrapValidator.min.js';
//import '../../../node_modules/smartadmin-plugins/bower_components/bootstrapvalidator/dist/js/bootstrapValidator.js';


export default class BootstrapValidator extends React.Component {

    componentDidMount() {
        $(findDOMNode(this)).bootstrapValidator(this.props.options || {})
    }

    render() {
        return (
            this.props.children
        )
    }
}