
export class ServiceResponse {
    IsSuccess: boolean;
    Message: string = "";
    Data: Array<any>;
    StatusCode: number;
}